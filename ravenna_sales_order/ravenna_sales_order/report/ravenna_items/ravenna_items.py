from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	columns, data = [], get_data()

	columns.append({
			"label": _("Ravenna Template"),
			"fieldname": "template",
			"fieldtype": "Link",
			"options":"Sales Order Template",
			"width": 120
		})
	columns.append({
			"label": _("Name Template"),
			"fieldname": "name_template",
			"fieldtype": "Data",
			"width": 120
		})
	columns.append({
			"label": _("ID Template"),
			"fieldname": "id_template",
			"fieldtype": "data",
			"width": 120
		})
	columns.append({
			"label": _("Date"),
			"fieldname": "date",
			"fieldtype": "date",
			"width": 120
		})
	columns.append({
			"label": _("Status"),
			"fieldname": "template_status",
			"fieldtype": "Select",
			"width": 120
		})
	items = frappe.get_list('Item',{"enable_on_template":1})
	for item in items:
		columns.append({
			"label": item.name,
			"fieldname": item.name.replace(" ", "_").lower(),
			"fieldtype": "Int",
			"width": 120
		})
	return columns, data
def get_data():
	data = []
	
	templates = frappe.get_list("Sales Order Template",fields=["name","name_template","id_template","date","template_status"])
	for template in templates:
		row = {}
		row["id"] = template.name
		row["template"] = template.name
		row["name_template"] = template.name_template
		row["id_template"] = template.id_template
		row["date"] = template.date
		row["template_status"] = template.template_status
		custom_items = frappe.db.sql("""select 
			ci.*
				from
			 `tabCustomer Items` ci
				where
				ci.parent='%s'
			 """ % template.name,as_dict=1)
		for item in  custom_items:
			row[item.item.replace(" ", "_").lower()] = item.quantity

		data.append(row)
	return data