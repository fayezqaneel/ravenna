// Copyright (c) 2016, Fayez Qandeel and contributors
// For license information, please see license.txt


frappe.query_reports["Ravenna Items"] = {
	onload:function(report){
		
		report.page.wrapper.find('.report-atb-sep-holder').hide();
		report.page.wrapper.find('.results').find(".result-area").show();
		report.page.wrapper.find('.msg-box.small').show();
	}
}
