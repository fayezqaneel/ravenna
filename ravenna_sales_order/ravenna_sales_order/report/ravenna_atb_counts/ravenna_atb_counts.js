// Copyright (c) 2016, Fayez Qandeel and contributors
// For license information, please see license.txt



frappe.query_reports["Ravenna ATB Counts"] = {
	onload:function(report){
		
		report.page.wrapper.find('.results').before("<div class='report-atb-sep-holder '><table ><tr><td id='left_col' width='50%'/><td  width='100'/><td width='50%' id='right_col' /></tr></table></div>");
		report.page.wrapper.find('.results').find(".result-area").hide();
		report.page.wrapper.find('.msg-box.small').hide();
		
		frappe.call({
	        method: 'ravenna_sales_order.ravenna_sales_order.report.ravenna_atb_counts.ravenna_atb_counts.get_report',
	        args: {
	            
	        },
	        callback: function(r) {
	        	
	        	var count = 0;
	            for(var i in r.message){
	            	var table = "";
	            	var atb = r.message[i];
	            	table +="<table class='table table-bordered'><tr class='new-atb'><td width='200'><b>"+atb.name+"</b></td><td><b>"+atb.count+"</b></td></tr>"
	            	for(var j in atb.subitems){
	            		var item = atb.subitems[j];
	            		table +="<tr><td>"+item.name+"</td><td>"+item.count+"</td></tr></table>"
	            	}
	            	var index = count%2;
	            	if(index==0){
	            		report.page.wrapper.find('.report-atb-sep-holder').find('#left_col').html(table);
	            	}else{
	            		report.page.wrapper.find('.report-atb-sep-holder').find('#right_col').html(table);
	            	}
	            	
	            	count +=1;
	            }
	            
	            
	        }
	    });
	}
}
