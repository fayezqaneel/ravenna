# Copyright (c) 2013, Fayez Qandeel and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	columns, data = [], []
	return columns, data
@frappe.whitelist()
def get_report(arg=None):
	"""get roles for a user"""
	atbs =  frappe.db.sql("select distinct(atb) as name from `tabCustomer Items` where atb is not null order by atb asc",as_dict=True)
	for atb in atbs:
		atb.count = frappe.db.sql("select sum(quantity) as count from `tabCustomer Items` where atb='%s'"%atb.name,as_dict=True)[0]["count"]
		atb.subitems = frappe.db.sql("select distinct(item) as name from `tabCustomer Items` where atb='%s'"%atb.name,as_dict=True)
		for newitem in atb.subitems:
			newitem.count = frappe.db.sql("select sum(quantity) as count from `tabCustomer Items` where atb='%s' and item='%s'"%(atb.name,newitem.name),as_dict=True)[0]["count"]

	return atbs