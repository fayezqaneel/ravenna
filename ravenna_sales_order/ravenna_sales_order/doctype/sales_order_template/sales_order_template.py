# -*- coding: utf-8 -*-
# Copyright (c) 2015, Fayez Qandeel and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class SalesOrderTemplate(Document):
	def validate(self):
		frappe.publish_realtime("hellow_world", {"doctype": self.doctype}, after_commit=True)

@frappe.whitelist()
def get_items(arg=None):
	"""get roles for a user"""
	return frappe.get_list('Item',{"enable_on_template":1})
@frappe.whitelist()
def get_customers(customer_group):
	"""get roles for a user"""
	customers = frappe.get_list('Customer',{"customer_group":customer_group})
	for customer in customers:
		customer["shipping_default_costs"] = frappe.get_list('Customer Cost Shipping',filters={"parent":customer.name},fields=["warehouse","cost_shipping"])
	return {
		"customers":customers,
		"warehouses":frappe.get_list('Warehouse'),
		"suppliers":frappe.get_list('Supplier')
	}

@frappe.whitelist()
def create_sales_order(template=None):
	"""get roles for a user"""
	import json
	default_shipping_account = frappe.db.get_single_value('Global Defaults', 'shipping_account_head')
	template_doc = frappe.get_doc("Sales Order Template",template);
	customers = {}
	for customer_item in template_doc.template_custom_items:
		if customer_item.customer in customers:
			customers[customer_item.customer]["items"].append({
					"item":customer_item.item,
					"customer":customer_item.customer,
					"quantity":customer_item.quantity,
					"cost_of_shipping":customer_item.cost_of_shipping
				})
		else:
			customers[customer_item.customer] = {"customer_name":customer_item.customer}
			customers[customer_item.customer]["items"] = []
			customers[customer_item.customer]["items"].append({
					"item":customer_item.item,
					"customer":customer_item.customer,
					"quantity":customer_item.quantity,
					"cost_of_shipping":customer_item.cost_of_shipping
				})
	print customers
	for customerkey,customer in customers.iteritems():
		cost_of_shipping = 0
		for  value in customer["items"]:
			cost_of_shipping += value["cost_of_shipping"]
		order = {}
		order["customer"] = customer["customer_name"]
		order["doctype"] = "Sales Order"
		order["delivery_date"] = template_doc.date
		order["items"] = []
		order["taxes"] = []
		order["taxes"].append({"doctype":"Sales Taxes and Charges","charge_type":"Actual","account_head":default_shipping_account,"tax_amount":cost_of_shipping,"description":"Cost of shipping"})
		for item in customer["items"]:
			order["items"].append({"doctype":"Sales Order Item","item_code":item["item"],"qty":item["quantity"]})
		frappe.get_doc(order).insert()
	return template_doc
	# for item in template_doc.template_items:
	# 	order = {}
	# 	order["customer"] = item.customer
	# 	order["doctype"] = "Sales Order"
	# 	order["delivery_date"] = template_doc.date
	# 	order["items"] = []
	# 	order["taxes"] = []
	# 	order["taxes"].append({"doctype":"Sales Taxes and Charges","charge_type":"Actual","account_head":template_doc.shipping_account_head,"tax_amount":item.cost_of_shipping,"description":"Cost of shipping"})
	# 	for customer_item in template_doc.template_custom_items:
	# 		if item.customer == customer_item.customer:
	# 			order["items"].append({"doctype":"Sales Order Item","item_code":customer_item.item,"qty":customer_item.quantity})
	# 	frappe.get_doc(order).insert()
	# return template_doc
