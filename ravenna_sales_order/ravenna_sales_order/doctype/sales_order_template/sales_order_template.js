// Copyright (c) 2016, Fayez Qandeel and contributors
// For license information, please see license.txt
//var items_timer=false;
frappe.intervals_manager = false;
$(window).on('hashchange', function() {
    frappe.intervals_manager.clearAll();
    //console.log(frappe.intervals_manager);
    if (window.location.hash.indexOf("Sales%20Order%20Template/") == -1 && window.location.hash.indexOf("Sales Order Template/") == -1) {
        $(".layout-side-section").show().next().removeClass("col-md-12").addClass("col-md-10");
        window.full_screen_mode = false;
        $('.container').removeClass("ravenna_container");
        $('.fullscreen_btn_').text("Full Screen");
        frm.page.wrapper.find('.menu-btn-group').show();

    } else {

        frappe.intervals_manager.rebuild();
        frm.page.wrapper.find('.menu-btn-group').hide();
        $(".layout-side-section").hide().next().removeClass("col-md-10").addClass("col-md-12");
    }
});




// frappe.realtime.on("hellow_world",function(data){
//   console.log("Fayezzzzorginal");
// });

frappe.ui.form.on('Sales Order Template', {
    setup: function(frm) {
      setTimeout(function() {
          if (frappe.socket) {
            frappe.socket.socket.on('dirottamenti_updates', function(data) {
                if(frm.doc.name==data.ravenna_sales_order){
                  var changes = {};
                  var changes_count = 0;
                  for(var i in data.dirottamenti_template_details){
                    var item = data.dirottamenti_template_details[i];
                    for(var j in frm.doc.template_custom_items){
                      var subitem = frm.doc.template_custom_items[j];
                      if(item.customer==subitem.customer && item.item==subitem.item && item.quantity!=subitem.quantity){
                        var change = {};
                        change["old_qty"] = subitem.quantity;
                        change["new_qty"] = item.quantity;
                        change["customer"] = item.customer;
                        change["item"] = item.item;
                        if(changes[item.customer]){
                          changes[item.customer].push(change);
                        }else{
                          changes[item.customer]=[change];
                        }
                        changes_count++;
                        subitem.quantity = item.quantity;
                      }
                    }
                  }
                  var changes_string = "";
                  for(var i in changes){
                    var customer = i;
                    changes_string +=" customer="+i+",";
                    for(var j in changes[i]){
                      var item = changes[i][j];
                      changes_string +=" item="+item.item;
                      changes_string +=" "+item.old_qty+"=>"+item.new_qty+",";
                    }
                  }

                  if(changes_count>0){
                    frm.items_editor.rebuild();
                    frm.refresh_fields();
                    frm.timeline.insert_comment("Workflow", " - "+data.user+" changed "+changes_string);
                    frm.timeline.refresh();
                    //frm.save();
                    changes_count = 0;
                  }
                }
            });
          }
        },0);

        frm.page.clear_inner_toolbar();
        frappe.intervals_manager = new frappe.intervals_manager(frm);
        $(function() {
            frm.page.wrapper.find('.menu-btn-group').hide()
            $(".layout-side-section").hide().next().removeClass("col-md-10").addClass("col-md-12");
            if ($('.fullscreen_btn_').length == 0) {
                $('.page-actions').find('.menu-btn-group').before('<span style="margin-right:5px;" class="fullscreen_btn_ btn btn-secondary btn-default btn-sm">Full Screen</span>');
            }
            window.full_screen_mode = false;
            $('.fullscreen_btn_').on('click', function() {


                if (!window.full_screen_mode) {
                    $(this).text("Close Full Screen");
                    window.full_screen_mode = true;
                } else {
                    $(this).text("Full Screen");
                    window.full_screen_mode = false;
                }
                $('.container').toggleClass("ravenna_container");
            });
            // $('.primary-action').on('click', function() {


            //     if (!window.full_screen_mode) {
            //         $(this).text("Close Full Screen");
            //         window.full_screen_mode = true;
            //     } else {
            //         $(this).text("Full Screen");
            //         window.full_screen_mode = false;
            //     }
            //     $('.container').toggleClass("ravenna_container");
            // });
        });
    },
    validate:function(frm){
        frm.timeline.insert_comment("Workflow", "- saved the template");
        frm.timeline.refresh();
    },

    refresh: function(frm) {

        var me = this;
        me.frm = frm;

        if (frm.items_editor) {

            frm.page.set_secondary_action(__("Create Order"), function() {
                frappe.call({
                    method: 'ravenna_sales_order.ravenna_sales_order.doctype.sales_order_template.sales_order_template.create_sales_order',
                    args: {
                        template: me.frm.doc.name
                    },
                    callback: function(r) {
                        if (r.message) {
                            msgprint(__("Sales orders successfully created check it out!"));
                            me.frm.set_value('template_status', 'Sales Order');
                            refresh_field("template_status");
                            me.frm.save();
                        }
                    }
                });

            });

        }
    },
    onload: function(frm) {
        var me = this;
        me.frm = frm;




        frm.page.set_secondary_action(__("Create Order"), function() {
            frappe.call({
                method: 'ravenna_sales_order.ravenna_sales_order.doctype.sales_order_template.sales_order_template.create_sales_order',
                args: {
                    template: me.frm.doc.name
                },
                callback: function(r) {
                    if (r.message) {
                        msgprint(__("Sales orders successfully created check it out!"));
                        me.frm.set_value('template_status', 'Sales Order');
                        refresh_field("template_status");
                        me.frm.save();
                    }
                }
            });

        });
        if (!frm.items_editor) {

            frm.items_editor = new frappe.ItemsEditor(frm, frm.fields_dict.template_customer_items_html.wrapper);

        }
        if (frm.items_editor && frm.doc.customer_group && frm.doc.customer_group != "") {
            frappe.call({
                method: 'ravenna_sales_order.ravenna_sales_order.doctype.sales_order_template.sales_order_template.get_customers',
                args: {
                    customer_group: frm.doc.customer_group
                },
                callback: function(r) {
                    if (r.message && r.message.customers && r.message.customers.length > 0) {
                        frm.items_editor.customers = r.message.customers;
                        $(document).trigger("customers_updated");
                    }
                    if (r.message && r.message.suppliers) {
                        frm.items_editor.suppliers = r.message.suppliers;
                    }
                    if (r.message && r.message.warehouses) {
                        frm.items_editor.warehouses = r.message.warehouses;
                    }
                }
            });
        }
    },
    customer_group: function(frm) {

        frappe.call({
            method: 'ravenna_sales_order.ravenna_sales_order.doctype.sales_order_template.sales_order_template.get_customers',
            args: {
                customer_group: frm.doc.customer_group
            },
            callback: function(r) {
                if (r.message && r.message.customers && r.message.customers.length > 0) {
                    frm.items_editor.customers = r.message.customers;
                    $(frm.items_editor.root_area).removeClass("withoutScroll").empty();
                    $(document).trigger("customers_updated");
                } else {
                    //console.log($(frm.items_editor.root_area));
                    $(frm.items_editor.root_area).addClass("withoutScroll").empty().text('No customers found...');
                }
                if (r.message && r.message.suppliers) {
                    frm.items_editor.suppliers = r.message.suppliers;
                }
                if (r.message && r.message.warehouses) {
                    frm.items_editor.warehouses = r.message.warehouses;
                }
            }
        });
    }
});
frappe.intervals_manager = Class.extend({
    init: function(frm) {

        this.frm = frm;
        var me = this;
        if (!this.intervals_array) {
            this.intervals_array = setInterval(function() {
                if (me.frm.doctype == "Sales Order Template") {
                    me.frm.save();
                    me.frm.timeline.insert_comment("Workflow", "- auto saved the template");
                    me.frm.timeline.refresh();
                }
            }, 60000);
        }
    },
    rebuild: function() {
        var me = this;
        if (!this.intervals_array) {
            this.intervals_array = setInterval(function() {
                if (me.frm.doctype == "Sales Order Template") {
                    me.frm.save();
                    me.frm.timeline.insert_comment("Workflow", "- auto saved the template");
                    me.frm.timeline.refresh();
                }
            }, 60000);
        }
    },
    clearAll: function() {
        clearInterval(this.intervals_array);
        this.intervals_array = false;
    }
});
frappe.ItemsEditor = Class.extend({
    init: function(frm, customers_area_root) {
        // this.party_field = frappe.ui.form.make_control({
        //     df: {
        //         "fieldtype": "Link",
        //         "options": "Customer",
        //         "label": "select customer",
        //         "fieldname": "customer_field_name",
        //         "placeholder": __("Select or add new customer")
        //     },
        //     parent: customers_area_root,
        //     only_input: true,
        // });

        // this.party_field.make_input();

        this.frm = frm;
        this.root_area = customers_area_root;
        var me = this;
        $(document).on("customers_updated", function() {
            $(customers_area_root).empty();
            var customers_area = $('<div class="form-grid"><div class="grid-heading-row"><div class="col grid-static-col col-xs-2 text-center" ><div class="static-area ellipsis">Cliente</div></div><div class="col grid-static-col col-xs-2  text-center "><div class="static-area ellipsis">Magazzino</div></div><div class="col grid-static-col col-xs-2 text-center" ><div class="static-area ellipsis">costo di spedizions</div></div></div><div class="grid-body"><div style="background:#fff" class="items-area"></div></div></div>')
                .appendTo(customers_area_root);

            $(customers_area_root).attr("id", "template_customer_items_html");
            me.wrapper = customers_area;
            me.make();
        });
    },
    rebuild: function() {
        $(this.root_area).find('.items-area').empty();
        $(this.root_area).find('.grid-heading-row .grid-row .col:not(:first)').remove();
        this.make();
    },
    make: function() {
        var me = this;
        frappe.call({
            method: 'ravenna_sales_order.ravenna_sales_order.doctype.sales_order_template.sales_order_template.get_items',
            callback: function(r) {
                items = r.message;
                me.items = items;
                if (!r.message) {
                    msgprint(__("Please enable on template at least one item"));
                    for (var i in me.frm.doc.template_items) {
                        var customer = me.frm.doc.template_items[i];
                        customer.summary_qty = 0;
                        customer.cost_of_shipping = 0;
                    }
                    refresh_field("template_items");
                    me.frm.doc.template_custom_items = [];
                    me.frm.save();
                }
                for (var i in items) {
                    me.wrapper.find(".grid-heading-row").append('<div class="col grid-static-col col-xs-2  text-center "><div class="static-area ellipsis">' + items[i]["name"] + '</div></div>');
                }
                me.wrapper.find(".grid-heading-row ").append('<div class="col grid-static-col col-xs-2  text-center "><div class="static-area ellipsis">Somma QTY</div></div>');
                me.wrapper.find(".grid-heading-row ").append('<div class="col grid-static-col col-xs-2  text-center "><div class="static-area ellipsis">Costo di Spedizione</div></div>');
                me.wrapper.find(".grid-heading-row ").append('<div class="col grid-static-col col-xs-2  text-center "><div class="static-area ellipsis">Orari</div></div>');

                me.wrapper.find(".grid-heading-row ").append('<div class="col grid-static-col col-xs-2  text-center "><div class="static-area ellipsis">Fornitore</div></div>');
                me.wrapper.find(".grid-heading-row ").append('<div class="col grid-static-col col-xs-2  text-center "><div class="static-area ellipsis">ATB</div></div>');



                var items_obj = {};
                var items_cost_shipping = {};
                var items_carrier_shipping = {};
                var items_warehouse = {};
                var items_supplier = {};
                var items_atb = {};

                for (var i in me.frm.doc.template_custom_items) {
                    var item = me.frm.doc.template_custom_items[i];
                    items_obj[item.customer + item.item] = item.quantity || 0;
                    items_cost_shipping[item.customer] = item.cost_shipping || 0;
                    items_carrier_shipping[item.customer] = item.carrier_shipping || '';
                    items_warehouse[item.customer] = item.warehouse || '';
                    items_supplier[item.customer] = item.supplier || '';
                    items_atb[item.customer] = item.atb || '';
                }


                for (var i in me.customers) {


                    var supplier_options = '<option value="">Select supplier</option>';

                    for (var j in me.suppliers) {

                        if (items_supplier[me.customers[i]["name"]] == me.suppliers[j]["name"]) {
                            supplier_options += '<option selected="selected" value="' + me.suppliers[j]["name"] + '">' + me.suppliers[j]["name"] + '</option>';
                        } else {
                            supplier_options += '<option  value="' + me.suppliers[j]["name"] + '">' + me.suppliers[j]["name"] + '</option>';
                        }
                    }

                    var suppliers_list = '<select class="customer-item  supplier-list form-control">' + supplier_options + '</select>';

                    var warehouse_options = '<option value="">Select warehouse</option>';
                    for (var j in me.warehouses) {
                        if (items_warehouse[me.customers[i]["name"]] == me.warehouses[j]["name"]) {
                            warehouse_options += '<option selected="selected" value="' + me.warehouses[j]["name"] + '">' + me.warehouses[j]["name"] + '</option>';
                        } else {
                            warehouse_options += '<option  value="' + me.warehouses[j]["name"] + '">' + me.warehouses[j]["name"] + '</option>';
                        }
                    }

                    var warehouses_list = '<select class="customer-item  warehouse-list form-control">' + warehouse_options + '</select>';
                    if (typeof items_cost_shipping[me.customers[i]['name']] == "undefined") {
                        items_cost_shipping[me.customers[i]['name']] = '';
                    }
                    var customer_html = '<div class="col grid-static-col col-xs-2 text-center" style="padding:5px 0px 0px 0px !important;"><div class="static-area ellipsis">' + me.customers[i]['name'] + '</div></div>';
                    customer_html += '<div class="col grid-static-col col-xs-2 text-center" data-fieldtype="Int"><div class="field-area" style="display: block;"><div class="form-group frappe-control input-max-width" >' + warehouses_list + '</div></div><div class="static-area ellipsis"></div></div>';
                    customer_html += '<div class="col grid-static-col col-xs-2 text-center" data-fieldtype="Int"><div class="field-area" style="display: block;"><div class="form-group frappe-control input-max-width" ><input  data-customer="' + me.customers[i]['name'] + '" type="text" autocomplete="off" class="input-with-feedback form-control bold input-sm customer-item-cost_shipping customer-item text-center"  value="' + items_cost_shipping[me.customers[i]['name']] + '" placeholder="0" data-col-idx="1"></div></div><div class="static-area ellipsis"></div></div>';
                    var total_quantity = 0;
                    for (var j in items) {
                        var quantity = items_obj[me.customers[i]['name'] + items[j]["name"]] || 0;

                        total_quantity += parseInt(quantity);

                        customer_html += '<div class="col grid-static-col col-xs-2 text-center" data-fieldtype="Int"><div class="field-area" style="display: block;"><div class="form-group frappe-control input-max-width" data-fieldtype="Int" data-fieldname="cost_shipping" title="cost_shipping"><input data-item="' + items[j]["name"] + '" data-customer="' + me.customers[i]['name'] + '" type="text" autocomplete="off" class="input-with-feedback form-control bold input-sm customer-item text-center item-withvalue"  value="' + quantity + '"  placeholder="0" data-doctype="Sales Order Template Item" data-col-idx="1"></div></div><div class="static-area ellipsis"></div></div>';
                    }
                    var total_cost = total_quantity * items_cost_shipping[me.customers[i]['name']];
                    customer_html += '<div class="col grid-static-col col-xs-2  text-center" data-fieldtype="Int"><div class="field-area" style="display: block;"><div class="form-group frappe-control input-max-width" ><input  data-customer="' + me.customers[i]['name'] + '" type="text" autocomplete="off" class="input-with-feedback form-control bold input-sm customer-item-summary_qty  text-center" disabled="disabled"  value="' + total_quantity + '" placeholder="0" data-col-idx="1"></div></div><div class="static-area ellipsis"></div></div>';
                    customer_html += '<div class="col grid-static-col col-xs-2 text-center" data-fieldtype="Int"><div class="field-area" style="display: block;"><div class="form-group frappe-control input-max-width" ><input  data-customer="' + me.customers[i]['name'] + '" type="text" autocomplete="off" class="input-with-feedback form-control bold input-sm customer-item-cost_of_shipping   text-center" disabled="disabled"  value="' + total_cost + '" placeholder="0" data-col-idx="1"></div></div><div class="static-area ellipsis"></div></div>';
                    if (typeof items_carrier_shipping[me.customers[i]['name']] == "undefined") {
                        items_carrier_shipping[me.customers[i]['name']] = '';
                    }
                    customer_html += '<div class="col grid-static-col col-xs-2 text-center" data-fieldtype="Int"><div class="field-area" style="display: block;"><div class="form-group frappe-control input-max-width" ><input  data-customer="' + me.customers[i]['name'] + '" type="text" autocomplete="off" class="input-with-feedback form-control bold input-sm customer-item-carrier_shipping text-center  customer-item "  value="' + items_carrier_shipping[me.customers[i]['name']] + '" placeholder="" data-col-idx="1"></div></div><div class="static-area ellipsis"></div></div>';





                    customer_html += '<div class="col grid-static-col col-xs-2 text-center" data-fieldtype="Int"><div class="field-area" style="display: block;"><div class="form-group frappe-control input-max-width" >' + suppliers_list + '</div></div><div class="static-area ellipsis"></div></div>';
                    customer_html += '<div class="col grid-static-col col-xs-2 text-center" data-fieldtype="Int"><div class="field-area" style="display: block;"><div class="form-group frappe-control input-max-width" ><input  data-customer="' + me.customers[i]['name'] + '" type="text" autocomplete="off" class="input-with-feedback form-control bold input-sm customer-item-atb text-center  customer-item "  value="' + items_atb[me.customers[i]['name']] + '" placeholder="" data-col-idx="1"></div></div><div class="static-area ellipsis"></div></div>';

                    me.wrapper.find(".items-area").append('<div class="editable_row">' + customer_html + '<div style="clear:both"></div></div>');
                }
                me.bind();
            }
        });
    },
    bind: function() {
        var me = this;
        this.wrapper.on("change", ".customer-item", function() {
            me.apply_change(this);
        });

    },
    apply_change: function(ele) {
        var me = this;


        var customer = $(ele).parents('.editable_row:first').find('.item-withvalue:first').attr("data-customer");
        var cost_shipping = $(ele).parents('.editable_row:first').find(".customer-item-cost_shipping:first").val();
        var carrier_shipping = $(ele).parents('.editable_row:first').find(".customer-item-carrier_shipping:first").val() || '';
        var warehouse = $(ele).parents('.editable_row:first').find(".warehouse-list option:selected").val() || '';
        var supplier = $(ele).parents('.editable_row:first').find(".supplier-list option:selected").val() || '';

        var atb = $(ele).parents('.editable_row:first').find(".customer-item-atb").val() || '';

        for (var i in this.customers) {
            if (this.customers[i].name == customer) {

                for (var j in this.customers[i].shipping_default_costs) {

                    if (this.customers[i].shipping_default_costs[j].warehouse == warehouse) {
                        //console.log(cost_shipping == '');
                        if (cost_shipping == '') {
                            //console.log(this.customers[i].shipping_default_costs[j].cost_shipping);
                            cost_shipping = this.customers[i].shipping_default_costs[j].cost_shipping;
                            $(ele).parents('.editable_row:first').find(".customer-item-cost_shipping:first").val(cost_shipping);
                        }
                    }
                }
            }
        }







        $(ele).parents('.editable_row:first').find('.item-withvalue').each(function() {
            var found = false;
            var item = $(this).attr('data-item');

            var quantity = $(this).val() || 0;
            for (var i in me.frm.doc.template_custom_items) {
                var customer_item = me.frm.doc.template_custom_items[i];

                if (customer_item.customer == customer && customer_item.item == item) {
                    customer_item.quantity = quantity;
                    customer_item.cost_shipping = cost_shipping;
                    customer_item.summary_qty = quantity;
                    customer_item.cost_of_shipping = customer_item.summary_qty * customer_item.cost_shipping;
                    customer_item.carrier_shipping = carrier_shipping;
                    customer_item.warehouse = warehouse;
                    customer_item.supplier = supplier;
                    customer_item.atb = atb;
                    found = true;
                }
            }

            if (!found) {
                var customer_item = frappe.model.add_child(me.frm.doc, "Customer Items", "template_custom_items");
                customer_item.customer = customer;
                customer_item.item = item;
                customer_item.quantity = quantity;
                customer_item.cost_shipping = cost_shipping;
                customer_item.summary_qty = quantity;
                customer_item.cost_of_shipping = customer_item.summary_qty * customer_item.cost_shipping;
                customer_item.carrier_shipping = carrier_shipping;
                customer_item.warehouse = warehouse;
                customer_item.supplier = supplier;
                customer_item.atb = atb;
            }
            refresh_field("template_custom_items");
        });





        var total_quantity = 0;
        var total_cost_of_shipping = 0;

        for (var i in me.frm.doc.template_custom_items) {
            var customer_item = me.frm.doc.template_custom_items[i];
            if (customer_item.customer == customer) {
                total_quantity += parseInt(customer_item.quantity);
                total_cost_of_shipping += parseInt(customer_item.cost_of_shipping);
            }
        }
        
        if(total_quantity>0){
            $(ele).parents('.editable_row:first').addClass("has_qty");
        }else{

            $(ele).parents('.editable_row:first').removeClass("has_qty");
        }
        $(ele).parents('.editable_row:first').find(".customer-item-summary_qty:first").val(total_quantity);
        $(ele).parents('.editable_row:first').find(".customer-item-cost_of_shipping:first").val(total_cost_of_shipping);
        refresh_field("template_custom_items");
    }
});
cur_frm.cscript.refresh = function(doc, dt, dn) {
    //console.log("hiiiiii");
};
// frappe.ui.form.on("Sales Order Template Item", "customer", function(frm, cdt, cdn) {
//     $(frm.fields_dict.template_customer_items_html.wrapper).empty();
//     var customers_area = $('<div class="form-grid"><div class="grid-heading-row"><div class="grid-row"><div class="col grid-static-col col-xs-2 text-left" data-fieldname="customer" data-fieldtype="Link"><div class="static-area ellipsis">Customer</div></div></div><div style="clear:both"></div></div><div class="grid-body"><div style="background:#fff" class="items-area"></div></div></div>')
//         .appendTo(frm.fields_dict.template_customer_items_html.wrapper);
//     frm.items_editor = new frappe.ItemsEditor(frm, customers_area);
// });
