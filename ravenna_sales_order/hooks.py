# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "ravenna_sales_order"
app_title = "RAVENNA Sales Order"
app_publisher = "Fayez Qandeel"
app_description = "RAVENNA Sales Order"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "customvivvo@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/ravenna_sales_order/css/ravenna_sales_order.css"
# app_include_js = "/assets/ravenna_sales_order/js/ravenna_sales_order.js"
app_include_css = "/assets/ravenna_sales_order/css/ravenna_sales_order.css"
# include js, css files in header of web template
# web_include_css = "/assets/ravenna_sales_order/css/ravenna_sales_order.css"
# web_include_js = "/assets/ravenna_sales_order/js/ravenna_sales_order.js"

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "ravenna_sales_order.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "ravenna_sales_order.install.before_install"
# after_install = "ravenna_sales_order.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "ravenna_sales_order.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"ravenna_sales_order.tasks.all"
# 	],
# 	"daily": [
# 		"ravenna_sales_order.tasks.daily"
# 	],
# 	"hourly": [
# 		"ravenna_sales_order.tasks.hourly"
# 	],
# 	"weekly": [
# 		"ravenna_sales_order.tasks.weekly"
# 	]
# 	"monthly": [
# 		"ravenna_sales_order.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "ravenna_sales_order.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "ravenna_sales_order.event.get_events"
# }

